import string
import requests
import config
import os
import xml.etree.ElementTree as ET

ENDPOINT = config.HOSTNAME + "admin.php"


if os.path.exists("input.txt"):
    print("Delete all users from input.txt(yes/no):")
    let = input()
    if let == 'no':
        exit()
    inputF = open("input.txt", "r")
    first = True
    for line in inputF:
        line = line.rstrip("\n\r")
        print(line)
        first = False
        POST = { 
            'password' : config.ADMINPASS, 
            'action':  'delete-user', 
            'user-name': line 
        }
        resp = requests.post(ENDPOINT, data = POST)
        mytree = ET.fromstring(resp.content)
        if mytree.text == '0': 
            print('!OK! Press ENTER to exit')
        else:
          print(resp.content)    

else:
    print('File input.txt not found in this directory. \nEnter name of user that you want to kill: \n')
    user = input()
    POST = { 
        'password' : config.ADMINPASS, 
        'action':  'delete-user', 
        'user-name': user 
    }
    resp = requests.post(ENDPOINT, data = POST)
    mytree = ET.fromstring(resp.content)
    if mytree.text == '0': 
      print('!OK! Press ENTER to exit')
    else:
      print(resp.content)

inputF.close()
input()