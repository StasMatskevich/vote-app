import random
import string
import hashlib
import requests
import config
import os
import xml.etree.ElementTree as ET

ENDPOINT = config.HOSTNAME + "admin.php"

if os.path.exists("input.txt") == 0:
    print("There is no input.txt file in this directory.")
    input()

input = open("input.txt", "r")
output = open("ouput.txt", "w")
symbols = string.ascii_letters + string.digits
first = True
for line in input:
    line = line.rstrip("\n\r")
    print(line)
    password = ""
    for i in range(8):
        password += random.choice(symbols)

    hash = hashlib.sha256(password.encode()).hexdigest()

    output.write(line)
    output.write('     ')
    output.write(password)
    output.write('\n')
    first = False
    POST = { 
        'password' : config.ADMINPASS, 
        'action':  'register', 
        'user-name': line, 
        'user-password': hash 
    }
    resp = requests.post(ENDPOINT, data = POST)
    mytree = ET.fromstring(resp.content)
    if mytree.text == '0': 
        print('OK')
    else:
      print(resp.content)

print("Press ENTER to exit")
input()
input.close()
output.close()