import string
import requests
import config
import xml.etree.ElementTree as ET

ENDPOINT = config.HOSTNAME + "admin.php"

print("Enter the id of vote that you want to delete. You can check it in get-results script: ")
line = input()
POST = { 
    'password' : config.ADMINPASS, 
    'action':  'delete-vote', 
    'id': line
}
resp = requests.post( ENDPOINT, data = POST)
mytree = ET.fromstring(resp.content)
if mytree.text == '0': 
        print('!OK! Press ENTER to exit')
else:
    print(resp.content)

input()