import requests
import config
import xml.etree.ElementTree as ET

ENDPOINT = config.HOSTNAME + "admin.php"

print("All users will delete (yes/no):")
let = input()

if let == 'yes':
    POST = { 
        'password' : config.ADMINPASS, 
        'action':  'drop-users' 
    }
    resp = requests.post( ENDPOINT, data = POST)
    mytree = ET.fromstring(resp.content)
    if mytree.text == '0': 
        print('!OK! Press ENTER to exit')
    else:
      print(resp.content)
    
    input()
else: 
    print("\nCancel\nPress Enter to Exit")
    input()
