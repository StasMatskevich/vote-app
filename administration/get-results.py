import requests
import config
import xml.etree.ElementTree as ET


def get_resp(ENDPOINT):
    POST = {
        'password' : config.ADMINPASS,
        'action':  'get-results'
    }
    resp = requests.post( ENDPOINT, data = POST)

    return resp


def main():
    ENDPOINT = config.HOSTNAME + "admin.php"

    mytree = ET.fromstring(get_resp(ENDPOINT).content)
    for child in mytree[0]:
        if child.tag == 'title':
            print('%-35s' % child.tag, end=" ")
        elif child.tag == 'id':
            print('%-5s' % child.tag, end=" ")
        else:
            print('%-15s' % child.tag, end=" ")
    print()
    for child in mytree:
        for x in child:
            if x.tag == 'title':
                print('%-35s' % x.text, end=" ")
            elif x.tag == 'id':
                print('%-5s' % x.text, end=" ")
            else:
                print('%-15s' % x.text, end=" ")
        print()

    input()



if __name__ == "__main__":
    main()
