import random
import string
import hashlib
import requests
import config
import os
import xml.etree.ElementTree as ET

ENDPOINT = config.HOSTNAME + "admin.php"

print('Enter the title of work that you want to add: ')
title = input()
print("\nEnter the uri of youtube video: ")
youtube = input()

print("\nEnter the substrate full path(./img/test.jpg): ")
img = input()
print("\nEnter the general information about video: ")
info = input()
print("\nEnter the end data of vote(2011-11-11): ")
end = input()

POST = { 
    'password' : config.ADMINPASS, 
    'action':  'add-vote', 
    #'image': open('./img/test.jpg', 'rb'),
    'title': title, 
    'video': youtube.replace('/watch?v=', '/embed/'), 
    'info' : info,
    'end': end,
}

files = {'image': open(img, 'rb')}

resp = requests.post(ENDPOINT, data = POST, files= files)
mytree = ET.fromstring(resp.content)
if mytree.text == '0': 
        print('!OK! Press ENTER to exit')
else:
      print(resp.content)

input()
