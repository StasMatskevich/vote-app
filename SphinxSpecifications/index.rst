.. VoteSystem documentation master file, created by
   sphinx-quickstart on Fri Nov 15 21:58:00 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VoteSystem's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./Specifications.md
   ./Manual.md
   ./Api.md
   ./Contacts.md
   
Indices and tables
==================

* :ref:`search`
