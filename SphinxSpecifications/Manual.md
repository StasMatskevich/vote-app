## Manual
### Overview

Our system can be deployed on different platforms like **Linux** or **Windows** with only one difference is setting up **web server**. If you want to help us solve this problem, you can complement our system with technology of [Docker Container](https://docs.docker.com/).

You can deploy this System wherever you want. In this Manual is considered the easiest option of installing the System on a free hosting for example www.000webhost.com.
### Set Up Free Hosting

Firstly you should sign in on this [site](https://www.000webhost.com/)

![](./img/sign_in.jpg)

Press button **Create New Site** and enter the name of your site and password optional. For example `physics-projects` or `vote-system`. **Don't forget to copy Password**

![](./img/create_new_site.jpg)

Open **File Manager** and upload all files except `Specifications`, `administration`, `git` folders and `.hitaccess`

![](./img/ftp.jpg)

### Set Up Databases

To set up databases you should go to **Database Manager** and create new Database. **Don't forget your password)**.

![](./img/database_create.jpg)

Next click **Manage** and **PhpMyAdmin**. Click to your database and open **SQL** to run SQL query. In our repository there is file named `init.sql` Just open it, copy everything, past there and click go. At the end you should see something like that.

![](./img/init_database.jpg)

Next open **File Manager** and change the `config.php` file. Enter you serveruser(dbuser) serverpass and dbname.

![](./img/config.jpg) 

To check system performance go to site of your **Vote System** and try user `test:test`. If everything is alright, your site is set up and work correctly.

### Administration

To **Administrate** you **Vote System** you need to install **Python 3** to your computer on [official site](https://www.python.org/).

After that clone this repository to you computer using the folowing command:

*Windows*:

    > git clone https://bitbucket.org/StasMatskevich/vote-app/src/

*Ubuntu*:
   
    $ sudo git clone https://bitbucket.org/StasMatskevich/vote-app/src/

Then open `administration` folder and change `HOSTNAME` and `ADMINPASS` in `config.py` file to yours.

#### Register

To **Register** users you should create `input.txt` file(to the same directory as `registration.py` script) with user names witch should be located like that

![](./img/input_for_register.jpg)

**!! Names must be Alphanumeric characters only !!**

Then run `registration.py` script, that build an `output.txt` file that contains added users and passwords.

#### Add Vote

To **Add Vote** run `add-vote.py` script and follow the instructions. If you want to check system performance you can use user `test:test`.

#### Deleting Users

To **Delete User** or **Users** we have 2 scripts. The first `delete-user.py` script delete 1 or more users. The second `drop-users.py` drop all users.

* If you want to delete only 1 user you start `delete-user.py` and and follow the instructions in the script. 

* If you want to delete more than one user at once you can create `input.txt` file in the same directory as a script and start it. User names should be located like that.

![](./img/input_for_register.jpg)

* If you want to drop all users you start `drop-users.py` script and relax.

#### Delete Vote

To **Delete Vote** we have 2 scripts. The first script `delete-vote.py` delete only 1 vote at once. The second `drop-vote.py` delete all votes.

* If you want do delete 1 vote start `delete-vote.py` script and follow the instructions inside.

* If you want to delete **All Votes** start `drop-votes.py` script **And Look How Heavens are Falling**.

#### Getting Results

To **Get Result** start `get-result.py` script and look for results. If you want to help us improve our system do the better table for results)