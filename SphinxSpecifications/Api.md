## API
To use API send POST requests to /admin route. Data flow is based on XML. 

**Common answers**:
0 - Ok
1 - Incorrect admin password
2 - Failed to connect to sql server
3 - failed to fetch sql query

### API Methods:

**register**
Registrate an user

Parameters:
*name* - name of user to registrate
*password* - SHA256 hash of user password

Answers:
All common answers

**delete-user**
Delete an user

Parameters:
*name* - name of user to delete

Answers:
All common answers

**drop-users**
Delete all users

No parameters

Answers:
All common answers

**get-results**
Returs list of nodes \<vote>, which consists of
*\<id>* - unique id of the vote
*\<title>* - vote title
*\<rate-science>*, *\<rate-science>*, *\<rate-science>* - final rate in the specified nomination
*\<rates-count>* - count of rates
*\<rate-final>* - final rate for this vote, calculated with rate-_final = (rate_science / 2 + rate_technic / 4 + rate_creative / 4) * (1 + count / 180)

No parameters

Answers:
All common answers, excluding 0(in this case, system will give a list as an answer)

**add-vote**
Add a vote

Parameters:
*title* - title of vote to add
*info* - description of vote to add
*video* - link to **youtube/rutube** video of vote to add
*image* - image, which will be displayed in the list of votes
*end* - date with time, when vote will be closed. Format: y-m-d h:i:s (e.g. 2020-12-25 00:00:00)

Answers:
All common answers
4 - empty *image*, can be a result of failed upload
5 - can't write *image* file, check server logs
6 - invalid *image*
7 - invalid *end*
8 - invalid *video* link

**set-end-date**
Sets *end* of all the votes to specified data

Parameters:
*end* - date with time, when all the votes will be closed. Format: y-m-d h:i:s (e.g. 2020-12-25 00:00:00)

Answers:
All common answers
7 - invalid *end*

**edit-vote**
Fully update vote details without losing rates

Parameters:
*title* - new vote title
*info* - new vote description
*video* - new link to **youtube/rutube** vote video 
*image* - new image, which will be displayed in the list of votes
*end* - new date with time, when vote will be closed. Format: y-m-d h:i:s (e.g. 2020-12-25 00:00:00)

Answers:
All common answers
4 - empty *image*, can be a result of failed upload
5 - can't write *image* file, check server logs
6 - invalid *image*
7 - invalid *end*
8 - invalid *video* link

**delete-vote**
Delete a vote

Parameters:
*id* - identifier of vote to delete. You can get it with *get-results*

Answers:
All common answers

**drop-votes**
Delete all votes

No parameters

Answers:
All common answers

---