## Specifications
---
### Stakeholders
* Teachers and students
* Our team)
### Capabilites and Functional
* Can start vote procedure.
* It can be aimed on different subjects. For example on phisics or something else.
* **User-Friendly Interface:** The   system   shall   provide   an   easy-to-use web user-interface.
* **Cross-Platform:** This system support different platforms.
* **Security:**
    * **Registration:**  User registration is performed using the system one person - one account.
    * **Anonymity:**  The system supports anonymous voting.
    * **Secrecy / Privacy**: No one is able to determine results of voting, until owner decides to publish them.
### Architecture
* Front-end default stack (HTML, CSS, JS, jQuery)
* Back-end (PHP + MySQL)
---