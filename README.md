# VoteSystem
---
### Stakeholders
* Teachers and students
* Our team)
### Capabilites and Functional
* Can start vote procedure.
* It can be aimed on different subjects. For example on phisics or something else.
* **User-Friendly Interface:** The system   provides an easy-to-use web user-interface.
* **Cross-Platform:** This system is supported on different platforms.
* **Security:**
    * **Registration:**  User registration is performed using the system one person - one account.
    * **Anonymity:**  Voting in the system is completely anonymous.
    * **Secrecy / Privacy**: No one is able to determine results of voting, until owner decides to publish them.
### Architecture
* Front-end default stack (HTML, CSS, JS, jQuery)
* Back-end (PHP + MySQL)


## Manual
---
### Overview

Our system can be deployed on different platforms like **Linux** or **Windows** with only one difference is setting up **web server**. If you want to help us solve this problem, you can complement our system with technology of [Docker Container](https://docs.docker.com/).

You can deploy this System wherever you want. In this Manual is considered the easiest option of installing the System on a free hosting for example www.000webhost.com.
### Set Up Free Hosting

Firstly you should sign in on this [site](https://www.000webhost.com/)

![](./Specifications/img/sign_in.jpg)

Press button **Create New Site** and enter the name of your site and password optional. For example `physics-projects` or `vote-system`. **Don't forget to copy Password**

![](./Specifications/img/create_new_site.jpg)

Open **File Manager** and upload all files except `Specifications`, `administration`, `git` folders and `.hitaccess`

![](./Specifications/img/ftp.jpg)

### Set Up Databases

To set up databases you should go to **Database Manager** and create new Database. **Don't forget your password)**.

![](./Specifications/img/database_create.jpg)

Next click **Manage** and **PhpMyAdmin**. Click to your database and open **SQL** to run SQL query. In our repository there is file named `init.sql` Just open it, copy everything, past there and click go. At the end you should see something like that.

![](./Specifications/img/init_database.jpg)

Next open **File Manager** and change the `config.php` file. Enter you serveruser(dbuser) serverpass and dbname.

![](./Specifications/img/config.jpg)

To check system performance go to site of your **Vote System** and try user `test:test`. If everything is alright, your site is set up and work correctly.

### Administration

To **Administrate** you **Vote System** you need to install python to your computer on [official site](https://www.python.org/).

After that clone this repository to you computer using the folowing command:

*Windows*:

    git clone something.git

*Ubuntu*:
   
    sudo git clone something.git

Then open `administation` folder and change `HOSTNAME` and `ADMINPASS` in `config.py` file to yours.

#### Registrate

To **Registrate** users you should create `input.txt` file(to the same directory as `registration.py` script) with user names wich should be located like that

![](./Specifications/img/input_for_register.jpg)

Then run `registration.py` script, that build an `output.txt` file that contains added users and passwords.

#### Add Vote

To **Add Vote** run `add-vote.py` script and follow the instructions. If you want to check system perfomance you can use user `test:test`.

#### Deleting Users

To **Delete User** or **Users** we have 2 scripts. The first `delete-user.py` script delete 1 or more users. The second `drop-users` drop all users.

* If you want to delete only 1 user you start `delete-user.py` and and follow the instructions in the script. 

* If you want to delete more than one user at once you can create `input.txt` file in the same directory as a script and start it. User names should be located like that.

![](./Specifications/img/input_for_register.jpg)

* If you want to drop all users you start `drop-users.py` script and relax.

#### Delete Vote

To **Delete Vote** we have 2 scripts. The first script `delete-vote.py` delete only 1 vote at once. The second `drop-vote.py` delete all votes.

* If you want do delete 1 vote start `delete-vote.py` script and follow the instructions inside.

* If you want to delete **All Votes** start `drop-votes.py` script **And Look How Heavens are Falling**.

#### Getting Results

To **Get Result** start `get-result.py` script and look for results. If you want to help us improve our system do the better table for results)

#### API
To use API send POST requests to /admin route. Data flow is based on XML. 

**Common answers**:
0 - Ok
1 - Incorrect admin password
2 - Failed to connect to sql server
3 - failed to fetch sql query

#### API Methods:

**register**
Registrate an user

Parameters:
*name* - name of user to registrate
*password* - SHA256 hash of user password

Answers:
All common answers

**delete-user**
Delete an user

Parameters:
*name* - name of user to delete

Answers:
All common answers

**drop-users**
Delete all users

No parameters

Answers:
All common answers

**get-results**
Returs list of nodes \<vote>, which consists of
*\<id>* - unique id of the vote
*\<title>* - vote title
*\<rate-science>*, *\<rate-science>*, *\<rate-science>* - final rate in the specified nomination
*\<rates-count>* - count of rates
*\<rate-final>* - final rate for this vote, calculated with rate-_final = (rate_science / 2 + rate_technic / 4 + rate_creative / 4) * (1 + count / 180)

No parameters

Answers:
All common answers, excluding 0(in this case, system will give a list as an answer)

**add-vote**
Add a vote

Parameters:
*title* - title of vote to add
*info* - description of vote to add
*video* - link to **youtube/rutube** video of vote to add
*image* - image, which will be displayed in the list of votes
*end* - date with time, when vote will be closed. Format: y-m-d h:i:s (e.g. 2020-12-25 00:00:00)

Answers:
All common answers
4 - empty *image*, can be a result of failed upload
5 - can't write *image* file, check server logs
6 - invalid *image*
7 - invalid *end*
8 - invalid *video* link

**set-end-date**
Sets *end* of all the votes to specified data

Parameters:
*end* - date with time, when all the votes will be closed. Format: y-m-d h:i:s (e.g. 2020-12-25 00:00:00)

Answers:
All common answers
7 - invalid *end*

**edit-vote**
Fully update vote details without losing rates

Parameters:
*title* - new vote title
*info* - new vote description
*video* - new link to **youtube/rutube** vote video 
*image* - new image, which will be displayed in the list of votes
*end* - new date with time, when vote will be closed. Format: y-m-d h:i:s (e.g. 2020-12-25 00:00:00)

Answers:
All common answers
4 - empty *image*, can be a result of failed upload
5 - can't write *image* file, check server logs
6 - invalid *image*
7 - invalid *end*
8 - invalid *video* link

**delete-vote**
Delete a vote

Parameters:
*id* - identifier of vote to delete. You can get it with *get-results*

Answers:
All common answers

**drop-votes**
Delete all votes

No parameters

Answers:
All common answers

---

## Fell free to contact us

Stas Matskevich [vk](https://vk.com/stas_matskevich)
Andrei Knyazhishche andrei.knyazhishche@gmail.com or [vk](https://vk.com/andrei.knyazhishche)