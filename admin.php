<?php 
    include 'config.php';

    $password = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
    if($password != $adminpass)
    {
        echo '<error>';
        echo '<code>1</code>';
        echo '<message>Incorrect password</message>';
        echo '</error>';
        return;
    }

    $conn = @mysqli_connect( $servername, $serveruser, $serverpass, $bdname);
	if (mysqli_connect_errno())
	{
		echo '<error>';
		echo '<code>2</code>';
		echo '<message>'.mysqli_connect_error().'</message>';
		echo '</error>';
		return;
    }

    
    if($_POST["action"] == "register")
	{
        $name = filter_var($_POST["user-name"], FILTER_SANITIZE_STRING);
        $password = filter_var($_POST["user-password"], FILTER_SANITIZE_STRING);

        $result = @mysqli_query($conn, "INSERT IGNORE INTO users 
        (name, password) 
        VALUES ('$name', '$password')");

        if(mysqli_errno($conn))
        {
            echo '<error>';
            echo '<code>3</code>';
            echo '<message>'.mysqli_error().'</message>';
            echo '</error>';
            return;
        }

        echo '<data>';
        echo 0;
        echo '</data>';
        return;
    }
    else if($_POST["action"] == "delete-user")
	{
        $name = filter_var($_POST["user-name"], FILTER_SANITIZE_STRING);

        $result = @mysqli_query($conn, "DELETE FROM users 
        WHERE name = '$name'");

        if(mysqli_errno($conn))
        {
            echo '<error>';
            echo '<code>3</code>';
            echo '<message>'.mysqli_error().'</message>';
            echo '</error>';
            return;
        }

        echo '<data>';
        echo 0;
        echo '</data>';
        return;
    }
    else if($_POST["action"] == "drop-users")
	{
        $result = @mysqli_query($conn, "DELETE FROM users 
        WHERE name != 'test'");

        if(mysqli_errno($conn))
        {
            echo '<error>';
            echo '<code>3</code>';
            echo '<message>'.mysqli_error().'</message>';
            echo '</error>';
            return;
        }

        echo '<data>';
        echo 0;
        echo '</data>';
        return;
    }
    else if($_POST["action"] == "get-results")
	{
        $result = @mysqli_query($conn, "SELECT id, title, rate_science, rate_technic, rate_creative, count, ((rate_science/2) + (rate_technic/4) + (rate_creative/4))*(1+ (count / 180)) as rate_final FROM votes ORDER BY rate_final DESC");

        if(mysqli_errno($conn))
        {
            echo '<error>';
            echo '<code>3</code>';
            echo '<message>'.mysqli_error().'</message>';
            echo '</error>';
            return;
        }

        echo '<data>';
		while($row = $result->fetch_assoc())
		{
            echo '<vote>';
            echo '<id>'.$row['id'].'</id>';
            echo '<title>'.$row['title'].'</title>';
            echo '<rate-science>'.$row['rate_science'].'</rate-science>';
            echo '<rate-technic>'.$row['rate_technic'].'</rate-technic>';
            echo '<rate-creative>'.$row['rate_creative'].'</rate-creative>';
            echo '<rates-count>'.$row['count'].'</rates-count>';
            echo '<rate-final>'.$row['rate_final'].'</rate-final>';
            echo '</vote>';
		}
        echo '</data>';
        return;
    }    
    else if($_POST["action"] == "add-vote")
	{
        if(!$_FILES['image'])
        {
            echo '<error>';
            echo '<code>4</code>';
            echo '<message>Invalid file</message>';
            echo '</error>';
            return;
        }

        $filename = $_FILES['image']['name'];

        $location = tempnam("images", "vote");
        $uploadOk = 1;
        $filetype = pathinfo($filename, PATHINFO_EXTENSION);

        $valid_types = array("jpg","jpeg","png");
        $valid = in_array(strtolower($filetype), $valid_types);
        if($valid)
        {
            if(!move_uploaded_file($_FILES['image']['tmp_name'], $location))
            {
                echo '<error>';
                echo '<code>5</code>';
                echo '<message>Can\'t write file</message>';
                echo '</error>';
                return;
            }
        }
        else
        {
            echo '<error>';
            echo '<code>6</code>';
            echo '<message>Invalid file</message>';
            echo '</error>';
            return;
        }

        $title = filter_var($_POST["title"], FILTER_SANITIZE_STRING);
        $video = filter_var($_POST["video"], FILTER_SANITIZE_STRING);
        $info = filter_var($_POST["info"], FILTER_SANITIZE_STRING);
        $timestr = filter_var($_POST["end"], FILTER_SANITIZE_STRING);
        $location = pathinfo($location, PATHINFO_FILENAME);

        if(($end = strtotime($timestr)) === false)
        {
            echo '<error>';
            echo '<code>7</code>';
            echo '<message>Invalid time</message>';
            echo '</error>';
            return;
        }
        $end = date('Y-m-d H:i:s', $end);

        if (!preg_match("/^https:\/\/www.youtube.com\/embed\/[0-9a-zA-Z\-]*$/", $video) && 
            !preg_match("/^https:\/\/rutube.ru\/play\/embed\/[0-9a-zA-Z\-]*$/", $video))
        {
            echo '<error>';
            echo '<code>8</code>';
            echo '<message>Bad youtube link</message>';
            echo '</error>';
            return;
        }

		$result = @mysqli_query($conn, "INSERT INTO votes 
        (title, image, youtube, info, end) 
        VALUES ('$title', '$location', '$video', '$info', '$end')");
        if(mysqli_errno($conn))
        {
            echo '<error>';
            echo '<code>3</code>';
            echo '<message>'.mysqli_error($conn).'</message>';
            echo '</error>';
            return;
        }

        echo '<data>';
        echo 0;
        echo '</data>';
        return;
    }
    else if($_POST["action"] == "set-end-date")
	{
        $timestr = filter_var($_POST["end"], FILTER_SANITIZE_STRING);

        if(($end = strtotime($timestr)) === false)
        {
            echo '<error>';
            echo '<code>7</code>';
            echo '<message>Invalid time</message>';
            echo '</error>';
            return;
        }
        $end = date('Y-m-d H:i:s', $end);

        $result = @mysqli_query($conn, "UPDATE votes SET end = '$end'");
        if(mysqli_errno($conn))
        {
            echo '<error>';
            echo '<code>3</code>';
            echo '<message>'.mysqli_error().'</message>';
            echo '</error>';
            return;
        }

        echo '<data>';
        echo 0;
        echo '</data>';
        return;
    }
    else if($_POST["action"] == "edit-vote")
	{
        if(!$_FILES['image'])
        {
            echo '<error>';
            echo '<code>3</code>';
            echo '<message>Invalid file</message>';
            echo '</error>';
            return;
        }

        $filename = $_FILES['image']['name'];

        $location = tempnam("images", "vote");
        $uploadOk = 1;
        $filetype = pathinfo($filename, PATHINFO_EXTENSION);

        $valid_types = array("jpg","jpeg","png");
        $valid = in_array(strtolower($filetype), $valid_types);
        if($valid)
        {
            if(!move_uploaded_file($_FILES['image']['tmp_name'], $location))
            {
                echo '<error>';
                echo '<code>4</code>';
                echo '<message>Can\'t write file</message>';
                echo '</error>';
                return;
            }
        }
        else
        {
            echo '<error>';
            echo '<code>5</code>';
            echo '<message>Invalid file</message>';
            echo '</error>';
            return;
        }

        $id = filter_var($_POST["vote-id"], FILTER_SANITIZE_STRING);
        $title = filter_var($_POST["title"], FILTER_SANITIZE_STRING);
        $video = filter_var($_POST["youtube"], FILTER_SANITIZE_STRING);
        $info = filter_var($_POST["info"], FILTER_SANITIZE_STRING);
        $timestr = filter_var($_POST["end"], FILTER_SANITIZE_STRING);
        $location = pathinfo($location, PATHINFO_FILENAME);

        if(($end = strtotime($timestr)) === false)
        {
            echo '<error>';
            echo '<code>6</code>';
            echo '<message>Invalid time</message>';
            echo '</error>';
            return;
        }
        $end = date('Y-m-d H:i:s', $end);

        if (!preg_match("/^https:\/\/www.youtube.com\/embed\/[0-9a-zA-Z\-]*$/", $video) && 
            !preg_match("/^https:\/\/rutube.ru\/play\/embed\/[0-9a-zA-Z\-]*$/", $video))
        {
            echo '<error>';
            echo '<code>7</code>';
            echo '<message>Bad youtube link</message>';
            echo '</error>';
            return;
        }

        $result = @mysqli_query($conn, "UPDATE votes SET 
        title = '$title', image = '$location', youtube = '$video', info = '$info', end = '$end'
        WHERE id = '$id'");
        if(mysqli_errno($conn))
        {
            echo '<error>';
            echo '<code>8</code>';
            echo '<message>'.mysqli_error($conn).'</message>';
            echo '</error>';
            return;
        }

        echo '<data>';
        echo 0;
        echo '</data>';
        return;
    }
    else if($_POST["action"] == "delete-vote")
	{
        $id = filter_var($_POST["id"], FILTER_SANITIZE_STRING);

        $result = @mysqli_query($conn, "DELETE FROM votes 
        WHERE id = '$id'");

        if(mysqli_errno($conn))
        {
            echo '<error>';
            echo '<code>3</code>';
            echo '<message>'.mysqli_error().'</message>';
            echo '</error>';
            return;
        }

        echo '<data>';
        echo 0;
        echo '</data>';
        return;
    }
    else if($_POST["action"] == "drop-votes")
	{
        $result = @mysqli_query($conn, "DELETE FROM votes");

        if(mysqli_errno($conn))
        {
            echo '<error>';
            echo '<code>3</code>';
            echo '<message>'.mysqli_error().'</message>';
            echo '</error>';
            return;
        }

        echo '<data>';
        echo 0;
        echo '</data>';
        return;
    }
?>