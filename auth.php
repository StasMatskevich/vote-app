<?php 
	include 'config.php';

	session_start(); 
	if($_POST["action"] == "auth")
	{
		$username = filter_var($_POST["username"], FILTER_SANITIZE_STRING);
		$password = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
		$password = hash("sha256", $password);

		$conn = @mysqli_connect( $servername, $serveruser, $serverpass, $bdname);
		if (mysqli_connect_errno())
		{
			echo '<error>';
			echo '<code>3</code>';
			echo '<message>'.mysqli_connect_error().'</message>';
			echo '</error>';
			return;
		}

		$result = @mysqli_query($conn, "SELECT id, name, password FROM users WHERE name = '$username'");
		if(mysqli_errno($conn))
		{
			echo '<error>';
			echo '<code>4</code>';
			echo '<message>'.mysqli_error($conn).'</message>';
			echo '</error>';
			return;
		}
		
		if (mysqli_num_rows($result) == 1) 
		{
			$row = mysqli_fetch_assoc($result);
			if($password == $row["password"]) 
			{
				mysqli_close($conn);    
				$_SESSION['login'] = "1";
				$_SESSION['name'] = $username;
				$_SESSION['id'] = $row["id"];
				setcookie("username", $username, time()+3600*24*30*6); 
				setcookie("password", $password, time()+3600*24*30*6); 
				echo '<data>';
				echo 0;
				echo '</data>';
				return;
			}
			else
			{
				echo '<data>';
				echo 1;
				echo '</data>';
				return;
			}
		}
		else
		{
			echo '<data>';
			echo 2;
			echo '</data>';
			return;
		}
	}
	else if($_POST["action"] == "logout")
	{
		$_SESSION['login'] = "0";
		setcookie('username', "", time()-100, '/');
		setcookie('password', "", time()-100, '/');
		echo '<data>';
		echo 0;
		echo '</data>';
	}
	else if($_POST["action"] == "cookie")
	{
		$username = isset($_COOKIE['username']) ? $_COOKIE['username'] : '';
		$password = isset($_COOKIE['password']) ? $_COOKIE['password'] : '';

		$username = filter_var($username, FILTER_SANITIZE_STRING);
		$password = filter_var($password, FILTER_SANITIZE_STRING);

		$conn = @mysqli_connect( $servername, $serveruser, $serverpass, $bdname);
		if (mysqli_connect_errno())
		{
			echo '<error>';
			echo '<code>3</code>';
			echo '<message>'.mysqli_connect_error().'</message>';
			echo '</error>';
			return;
		}

		$result = @mysqli_query($conn, "SELECT id, name, password FROM users WHERE name = '$username'");
		if(mysqli_errno($conn))
		{
			echo '<error>';
			echo '<code>4</code>';
			echo '<message>'.mysqli_error($conn).'</message>';
			echo '</error>';
			return;
		}

		if (mysqli_num_rows($result) == 1) 
		{
			$row = mysqli_fetch_assoc($result);
			if($password == $row["password"]) 
			{
				mysqli_close($conn);    
				$_SESSION['login'] = "1";
				$_SESSION['name'] = $username;
				$_SESSION['id'] = $row["id"];
				echo '<data>';
				echo 0;
				echo '</data>';
				return;
			}
			else
			{
				echo '<data>';
				echo 1;
				echo '</data>';
				return;
			}
		}
		else
		{
			echo '<data>';
			echo 2;
			echo '</data>';
			return;
		}
	}
?>