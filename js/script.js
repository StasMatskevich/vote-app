var activeSlide = 1;
var lastVote = 0;
var voteTimer;
var rates = {};
var backAction;

$(document).ready(function () {
	$.ajax({
		type: 'POST',
		url: 'auth.php',
		dataType: "xml",
		data: {
			'action': 'cookie'
		},
		success: function (data, textStatus, request) {
			if (ValidateResponse(data)) {
				if ($(data).children('error').length > 0) {
					console.log("Couldn't validate cookie", $(data).children('error').children('code').text(), $(data).children('error').children('message').text());
					ShowSlide(LoginSlide());
				}
				else
					if ($(data).children('data').text() == '0')
						GetVotes();
					else {
						console.log("Couldn't validate cookie");
						ShowSlide(LoginSlide());
					}
			}
			else {
				console.log("Bad answer from cookie validation");
				ShowSlide(LoginSlide());
			}
		},
		error: function (request, textStatus, errorThrown) {
			OnRequestFailed('-11', 'Cookie check error: ' + errorThrown, true);
			ShowSlide(LoginSlide());
		}
	});
});

function LoginSlide() {
	return `					
		<form class="login-form" onsubmit="return SubmitLogin()">
			<span>Вход</span>
			<hr>
			<span class="login-error"></span>
			<input class="username-input" type="text" placeholder="Имя пользователя" required>
			<input class="password-input" type="password" placeholder="Пароль" required>
			<input type="submit" value="Войти">
			<div class="login-troubles">
				<a href="#" onclick="ShowSlide(ContactAdministrationSlide())">Проблемы с системой?</a>
			</div>
		</form>`;
}

function ContactAdministrationSlide() {
	return `
		<span>Свяжитесь с администрацией:</span>
		<span>st.matskevich2013@ya.ru</span>
		<a href="#" onclick="ShowSlide(LoginSlide())" style="color: #4285F4">Вернуться назад</a>`;
}

function VoteRecordedSlide() {
	return `
		<span>Спасибо! Ваш голос записан!</span>
		<a href="#" onclick="GetVotes()" style="color: #4285F4">Показать другие голосования</a>`;
}

function ShowSlide(slide) {
	if ($('.slide[data-id="' + activeSlide + '"] iframe').length > 0)
		$('.slide[data-id="' + activeSlide + '"] iframe').attr('src', $('.slide[data-id="' + activeSlide + '"] iframe').attr('src'));
	$('.slide[data-id="' + ((activeSlide + 2) % 3) + '"]').removeClass('left').addClass('right');
	$('.slide[data-id="' + ((activeSlide + 1) % 3) + '"]').removeClass('right').html(slide);
	$('.slide[data-id="' + activeSlide + '"]').addClass('left');
	activeSlide = (activeSlide + 1) % 3;
}

function ToggleLogOut(toggle) {
	if (toggle)
		$('#header').addClass('logout');
	else
		$('#header').removeClass('logout');
}

function ToggleBack(toggle) {
	if (toggle)
		$('#header').addClass('back');
	else
		$('#header').removeClass('back');
}

function Back() {
	$('#hider').addClass('active');
	backAction();
}

function ClickVote(vote) {
	lastVote = $(vote).attr('data-id');
	GetVoteDetails($(vote).attr('data-id'));
}

function VoteTimerUpdate() {
	if ($('.slide[data-id="' + activeSlide + '"] .timer').length < 1) return;
	if (parseInt($('.slide[data-id="' + activeSlide + '"] .timer').attr('data-time')) * 1000 < Date.now()) {
		$('.slide[data-id="' + activeSlide + '"] .timer').text('Голосование завершено');
		clearInterval(voteTimer);
	}
	else {
		var diff = parseInt($('.slide[data-id="' + activeSlide + '"] .timer').attr('data-time')) * 1000 - Date.now();
		var secs = Math.floor(diff / 1000);
		var mins = Math.floor(secs / 60);
		var hours = Math.floor(mins / 60);
		var days = Math.floor(hours / 24);
		secs %= 60;
		mins %= 60;
		hours %= 24;
		$('.slide[data-id="' + activeSlide + '"] .timer').text('До конца голосования:\n' + days + ' суток\n' + hours + ' часов ' + mins + ' минут ' + secs + ' секунд');
	}
}

function SetRates() {
	rates['science'] = 3;
	rates['technic'] = 3;
	rates['creative'] = 3;
	$('.slide[data-id="' + activeSlide + '"] .rate').click(function () {
		$(this).parent().children().removeClass('active');
		$(this).addClass('active');
		rates[$(this).attr('data-category')] = $(this).attr('data-rate');
		switch ($(this).attr('data-rate')) {
			case ('5'):
				$(this).parent().parent().find('.comment').text('Супер!');
				break;
			case ('4'):
				$(this).parent().parent().find('.comment').text('Очень хорошо');
				break;
			case ('3'):
				$(this).parent().parent().find('.comment').text('Хорошо');
				break;
			case ('2'):
				$(this).parent().parent().find('.comment').text("Неплохо");
				break;
			case ('1'):
				$(this).parent().parent().find('.comment').text('Можно и лучше');
				break;
		}
	});
}

function ValidateResponse(response) {
	if ($(response).children('error').length < 1 && $(response).children('data').length < 1)
		return false;
	return true;
}

function OnRequestFailed(code, error, login) {
	if (error != undefined) console.log(error);
	if (login != true) ShowSlide(LoginSlide());
	$('#hider').removeClass('active');
	ToggleLogOut(false);
	ToggleBack(false);
	$('.slide[data-id="' + activeSlide + '"] .login-error').text("Произошла системная ошибка. Мы знаем об этом и уже работаем над исправлением(Код ошибки: " + code + ")").slideDown();
}

function SubmitLogin() {
	$('#hider').addClass('active');
	$.ajax({
		type: 'POST',
		url: 'auth.php',
		dataType: "xml",
		data: {
			'action': 'auth',
			'username': $('.slide[data-id="' + activeSlide + '"] .username-input').val(),
			'password': $('.slide[data-id="' + activeSlide + '"] .password-input').val()
		},
		success: function (data, textStatus, request) {
			if (ValidateResponse(data)) {
				if ($(data).children('error').length > 0)
					OnRequestFailed($(data).children('error').children('code').text(), $(data).children('error').children('message').text(), true);
				else {
					if ($(data).children('data').text() == '0')
						GetVotes();
					else if ($(data).children('data').text() == '1') {
						$('#hider').removeClass('active');
						$('.slide[data-id="' + activeSlide + '"] .login-error').text('Неверный пароль').slideDown();
					}
					else if ($(data).children('data').text() == '2') {
						$('#hider').removeClass('active');
						$('.slide[data-id="' + activeSlide + '"] .login-error').text("Пользователя не существует").slideDown();
					}
				}
			}
			else {
				OnRequestFailed('-1', 'Login response validation error', true);
			}
		},
		error: function (request, textStatus, errorThrown) {
			OnRequestFailed('-2', 'Login request error: ' + errorThrown, true);
		}
	});
	return false;
}

function GetVotes() {
	$('#hider').addClass('active');
	$.ajax({
		type: 'POST',
		url: 'vote.php',
		dataType: "xml",
		data: {
			'action': 'get-votes'
		},
		success: function (data, textStatus, request) {
			if (ValidateResponse(data)) {
				if ($(data).children('error').length > 0)
					OnRequestFailed($(data).children('error').children('code').text(), $(data).children('error').children('message').text());
				else {
					$('#hider').removeClass('active');
					ToggleLogOut(true);
					ToggleBack(false);
					ShowSlide($(data).children('data').text());
				}
			}
			else {
				OnRequestFailed('-3', 'Votes response validation error');
			}
		},
		error: function (request, textStatus, errorThrown) {
			OnRequestFailed('-4', 'Votes request error: ' + errorThrown);
		}
	});
}

function GetVoteDetails(id) {
	$('#hider').addClass('active');
	$.ajax({
		type: 'POST',
		url: 'vote.php',
		dataType: "xml",
		data: {
			'action': 'get-vote-details',
			'id': id
		},
		success: function (data, textStatus, request) {
			if (ValidateResponse(data)) {
				if ($(data).children('error').length > 0)
					OnRequestFailed($(data).children('error').children('code').text(), $(data).children('error').children('message').text());
				else {
					$('#hider').removeClass('active');
					ShowSlide($(data).children('data').text());
					ToggleBack(true);
					backAction = GetVotes;
					voteTimer = setInterval(VoteTimerUpdate, 1000);
					VoteTimerUpdate();
					SetRates();
				}
			}
			else {
				OnRequestFailed('-5', 'Vote details response validation error');
			}
		},
		error: function (request, textStatus, errorThrown) {
			OnRequestFailed('-6', 'Vote details request error: ' + errorThrown);
		}
	});
}

function LogOut() {
	$('#hider').addClass('active');
	$.ajax({
		type: 'POST',
		url: 'auth.php',
		dataType: "xml",
		data: {
			'action': 'logout'
		},
		success: function (data, textStatus, request) {
			if (ValidateResponse(data)) {
				if ($(data).children('error').length > 0)
					OnRequestFailed($(data).children('error').children('code').text(), $(data).children('error').children('message').text());
				else {
					$('#hider').removeClass('active');
					ToggleLogOut(false);
					ToggleBack(false);
					ShowSlide(LoginSlide());
				}
			}
			else {
				OnRequestFailed('-7', 'Log out response validation error');
			}
		},
		error: function (request, textStatus, errorThrown) {
			OnRequestFailed('-8', 'Log out request error: ' + errorThrown);
		}
	});
}

function Vote() {
	$('#hider').addClass('active');
	$.ajax({
		type: 'POST',
		url: 'vote.php',
		dataType: "xml",
		data: {
			'action': 'vote',
			'id': lastVote,
			'science': rates['science'],
			'technic': rates['technic'],
			'creative': rates['creative']
		},
		success: function (data, textStatus, request) {
			if (ValidateResponse(data)) {
				if ($(data).children('error').length > 0)
					OnRequestFailed($(data).children('error').children('code').text(), $(data).children('error').children('message').text());
				else {
					$('#hider').removeClass('active');
					ShowSlide(VoteRecordedSlide());
					backAction = function () { GetVoteDetails(lastVote) };
				}
			}
			else {
				OnRequestFailed('-9', 'Vote response validation error');
			}
		},
		error: function (request, textStatus, errorThrown) {
			OnRequestFailed('-10', 'Vote request error: ' + errorThrown);
		}
	});
}