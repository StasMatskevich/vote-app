CREATE TABLE `users` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` CHAR(64) NOT NULL,
	`password` VARCHAR(256) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name` (`name`)
);
CREATE TABLE `votes` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(128) NOT NULL DEFAULT '',
	`image` VARCHAR(256),
	`youtube` VARCHAR(256),
	`info` VARCHAR(4096),
	`rate_science` FLOAT(4,2) UNSIGNED NOT NULL DEFAULT '0.00',
	`rate_technic` FLOAT(4,2) UNSIGNED NOT NULL DEFAULT '0.00',
	`rate_creative` FLOAT(4,2) UNSIGNED NOT NULL DEFAULT '0.00',
	`count` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`start` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`end` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);
CREATE TABLE `rates` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user` INT(11) UNSIGNED NOT NULL,
	`vote` INT(11) UNSIGNED NOT NULL,
	`rate_science` FLOAT(4,2) UNSIGNED NOT NULL,
	`rate_technic` FLOAT(4,2) UNSIGNED NOT NULL,
	`rate_creative` FLOAT(4,2) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `user` (`user`, `vote`)
);
DELIMITER $$
CREATE TRIGGER `add_rate` AFTER INSERT ON `rates` FOR EACH ROW BEGIN
	IF @TRIGGER_DISABLED IS NULL OR @TRIGGER_DISABLED = 0 THEN
		UPDATE votes SET count = count + 1,
			rate_science = CASE WHEN count > 0 THEN (rate_science * (count - 1) + new.rate_science) / count ELSE 0 END,
			rate_technic = CASE WHEN count > 0 THEN (rate_technic * (count - 1) + new.rate_technic) / count ELSE 0 END,
			rate_creative = CASE WHEN count > 0 THEN (rate_creative * (count - 1) + new.rate_creative) / count ELSE 0 END WHERE id = new.vote;
	END IF;
END;$$
CREATE TRIGGER `update_rate` AFTER UPDATE ON `rates` FOR EACH ROW BEGIN
	IF @TRIGGER_DISABLED IS NULL OR @TRIGGER_DISABLED = 0 THEN
		IF old.vote = new.vote THEN
			UPDATE votes SET rate_science = CASE WHEN count > 0 THEN rate_science + (new.rate_science - old.rate_science) / count ELSE 0 END,
				rate_technic = CASE WHEN count > 0 THEN rate_technic + (new.rate_technic - old.rate_technic) / count ELSE 0 END,
				rate_creative = CASE WHEN count > 0 THEN rate_creative + (new.rate_creative - old.rate_creative) / count ELSE 0 END WHERE id = new.vote;
		ELSE
			UPDATE votes SET count = count - 1, 
				rate_science = CASE WHEN count > 0 THEN (rate_science * (count + 1) - old.rate_science) / count ELSE 0 END,
				rate_technic = CASE WHEN count > 0 THEN (rate_technic * (count + 1) - old.rate_technic) / count ELSE 0 END,
				rate_creative = CASE WHEN count > 0 THEN (rate_creative * (count + 1) - old.rate_creative) / count ELSE 0 END WHERE id = old.vote;
			UPDATE votes SET count = count + 1,
				rate_science = CASE WHEN count > 0 THEN (rate_science * (count - 1) + new.rate_science) / count ELSE 0 END,
				rate_technic = CASE WHEN count > 0 THEN (rate_technic * (count - 1) + new.rate_technic) / count ELSE 0 END,
				rate_creative = CASE WHEN count > 0 THEN (rate_creative * (count - 1) + new.rate_creative) / count ELSE 0 END WHERE id = new.vote;
		END IF;
	END IF;
END;$$
CREATE TRIGGER `delete_rate` AFTER DELETE ON `rates` FOR EACH ROW BEGIN
	IF @TRIGGER_DISABLED IS NULL OR @TRIGGER_DISABLED = 0 THEN
		UPDATE votes SET count = count - 1, 
			rate_science = CASE WHEN count > 0 THEN (rate_science * (count + 1) - old.rate_science) / count ELSE 0 END,
			rate_technic = CASE WHEN count > 0 THEN (rate_technic * (count + 1) - old.rate_technic) / count ELSE 0 END,
			rate_creative = CASE WHEN count > 0 THEN (rate_creative * (count + 1) - old.rate_creative) / count ELSE 0 END WHERE id = old.vote;
	END IF;
END;$$
CREATE TRIGGER `delete_user` AFTER DELETE ON `users` FOR EACH ROW BEGIN
	IF @TRIGGER_DISABLED IS NULL OR @TRIGGER_DISABLED = 0 THEN
		DELETE FROM rates WHERE user = old.id;
	END IF;
END;$$
CREATE TRIGGER `delete_vote` AFTER DELETE ON `votes` FOR EACH ROW BEGIN
	IF @TRIGGER_DISABLED IS NULL OR @TRIGGER_DISABLED = 0 THEN
		SET @TRIGGER_DISABLED = 1;
		DELETE FROM rates WHERE vote = old.id;
		SET @TRIGGER_DISABLED = 0;
	END IF;
END;$$
DELIMITER ;
INSERT INTO `users` (`name`, `password`) VALUES ('test', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08');