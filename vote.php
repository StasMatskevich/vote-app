<?php 
	include 'config.php';

	session_start();
	if(!isset($_SESSION['login']) || $_SESSION['login'] == 0 || $_SESSION['login'] == '0') 
	{
		echo '<error>';
		echo '<code>1</code>';
		echo '<message>Not logged in</message>';
		echo '</error>';
		return;
	}

	$conn = @mysqli_connect( $servername, $serveruser, $serverpass, $bdname);
	if (mysqli_connect_errno())
	{
		echo '<error>';
		echo '<code>2</code>';
		echo '<message>'.mysqli_connect_error().'</message>';
		echo '</error>';
		return;
	}

	if($_POST["action"] == "get-votes")
	{
		$result = @mysqli_query($conn, "SELECT id, title, image FROM votes");
		if(mysqli_errno($conn))
		{
			echo '<error>';
			echo '<code>3</code>';
			echo '<message>'.mysqli_error($conn).'</message>';
			echo '</error>';
			return;
		}	

		if (mysqli_num_rows($result) < 1) 
		{
			echo '<data>';
			$data = '<span>Нет доступных голосований. Попробуйте снова чуть позже</span>';
			$data = $data.'<a href="#" onclick="GetVotes()" style="color: #4285F4">Попробовать снова</a>';
			echo filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
			echo '</data>';
			return;
		}

		echo '<data>';
		$data = '<div class="votes-container">';
		while($row = $result->fetch_assoc())
		{
			$data = $data.'<div class="vote" data-id="'.$row['id'].'" onclick="ClickVote(this)">';

			if(!empty($row['image'])) $data = $data.'<div class="background" style="background-image:url(images/'.$row['image'].')"></div>';
			else $data = $data.'<div class="background" style="background-image:url(images/default.png)"></div>';

			$data = $data.'<div class="text">'.$row['title'].'</div>';
			$data = $data.'</div>';
		}
		$data = $data.'</div>';
		echo filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
		echo '</data>';
	}
	if($_POST["action"] == "get-vote-details")
	{
		$id = filter_var($_POST["id"], FILTER_SANITIZE_STRING);
		$user = $_SESSION['id'];
		$first_time = true;
		$old_science = 0;
		$old_technic = 0;
		$old_creative = 0;
		$result = @mysqli_query($conn, "SELECT rate_science, rate_technic, rate_creative FROM rates WHERE vote = '$id' AND user = '$user'");
		if(mysqli_errno($conn))
		{
			echo '<error>';
			echo '<code>3</code>';
			echo '<message>'.mysqli_error($conn).'</message>';
			echo '</error>';
			return;
		}	

		if (mysqli_num_rows($result) == 1) 
		{
			$row = mysqli_fetch_assoc($result);
			$first_time = false;
			$old_science = $row['rate_science'];
			$old_technic = $row['rate_technic'];
			$old_creative = $row['rate_creative'];
		}

		$result = @mysqli_query($conn, "SELECT title, image, youtube, info, start, end FROM votes WHERE id = '$id'");
		if(mysqli_errno($conn))
		{
			echo '<error>';
			echo '<code>3</code>';
			echo '<message>'.mysqli_error($conn).'</message>';
			echo '</error>';
			return;
		}	

		if (mysqli_num_rows($result) == 1) 
		{
			$row = mysqli_fetch_assoc($result);

			echo '<data>';
			$data = '<div class="vote-wrapper">';

			$data = $data.'<div class="vote selected">';
			if(!empty($row['image'])) $data = $data.'<div class="background" style="background-image:url(images/'.$row['image'].')"></div>';
			else $data = $data.'<div class="background" style="background-image:url(images/default.png)"></div>';
			$data = $data.'<div class="text">'.$row['title'].'</div>';
			$data = $data.'</div>';

			if(!empty($row['youtube']))
			{
				$data = $data.'<div class="youtube">';
				$data = $data.'<iframe frameborder="0" src="'.$row['youtube'].'"></iframe>';
				$data = $data.'</iframe>';
				$data = $data.'</div>';
			}
			if(!empty($row['info'])) $data = $data.'<div class="info">'.$row['info'].'</div>';

			if($_SESSION['name'] != $testlogin)
			{
				$data = $data.'<div class="vote-title">Голосование</div>';

				$data = $data.'<div class="timer" data-time="'.strtotime($row['end']).'"></div>';

				$start = strtotime($row['start']);
				$end = strtotime($row['end']);
				if(time() > $start && time() < $end)
				{
					$data = $data.'<div class="vote-old-rates">';
					if($first_time) $data = $data.'Вы ещё не голосовали';
					else {
						$data = $data.'Вы уже голосовали. Предыдущая оценка:';
						$data = $data.'<br>Научная часть: '.intval($old_science);
						$data = $data.'<br>Техническая сложность: '.intval($old_technic);
						$data = $data.'<br>Креативность проекта: '.intval($old_creative);
					}
					$data = $data.'</div>';

					$data = $data.'<div class="vote-info">Оцените работу участников по трём критериям:</div>';

					$data = $data.'<div class="category">';
					$data = $data.'<div class="name">Научная часть</div>';
					$data = $data.'<div class="rate-wrapper green">';
					$data = $data.'<div class="rate" data-rate="5" data-category="science">5</div>';
					$data = $data.'<div class="rate" data-rate="4" data-category="science">4</div>';
					$data = $data.'<div class="rate active" data-rate="3" data-category="science">3</div>';
					$data = $data.'<div class="rate" data-rate="2" data-category="science">2</div>';
					$data = $data.'<div class="rate" data-rate="1" data-category="science">1</div>';
					$data = $data.'</div>';
					$data = $data.'<div class="comment">Хорошо</div>';
					$data = $data.'</div>';

					$data = $data.'<div class="category">';
					$data = $data.'<div class="name">Техническая сложность</div>';
					$data = $data.'<div class="rate-wrapper red">';
					$data = $data.'<div class="rate" data-rate="5" data-category="technic">5</div>';
					$data = $data.'<div class="rate" data-rate="4" data-category="technic">4</div>';
					$data = $data.'<div class="rate active" data-rate="3" data-category="technic">3</div>';
					$data = $data.'<div class="rate" data-rate="2" data-category="technic">2</div>';
					$data = $data.'<div class="rate" data-rate="1" data-category="technic">1</div>';
					$data = $data.'</div>';
					$data = $data.'<div class="comment">Хорошо</div>';
					$data = $data.'</div>';

					$data = $data.'<div class="category">';
					$data = $data.'<div class="name">Креативность проекта</div>';
					$data = $data.'<div class="rate-wrapper blue">';
					$data = $data.'<div class="rate" data-rate="5" data-category="creative">5</div>';
					$data = $data.'<div class="rate" data-rate="4" data-category="creative">4</div>';
					$data = $data.'<div class="rate active" data-rate="3" data-category="creative">3</div>';
					$data = $data.'<div class="rate" data-rate="2" data-category="creative">2</div>';
					$data = $data.'<div class="rate" data-rate="1" data-category="creative">1</div>';
					$data = $data.'</div>';
					$data = $data.'<div class="comment">Хорошо</div>';
					$data = $data.'</div>';

					$data = $data.'<button onclick="Vote()">Отправить голос</button>';
				}
			}
			else
			{
				$data = $data.'<div class="vote-title">С тестовой учётной записи голосовать нельзя</div>';
			}
			$data = $data.'</div>';
			echo filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
			echo '</data>';
		}
		else
		{
			echo '<error>';
			echo '<code>4</code>';
			echo '<message>Bad MySQL response</message>';
			echo '</error>';
			return;
		}

	}
	else if($_POST["action"] == "vote")
	{
		$science = filter_var($_POST["science"], FILTER_SANITIZE_STRING);
		$technic = filter_var($_POST["technic"], FILTER_SANITIZE_STRING);
		$creative = filter_var($_POST["creative"], FILTER_SANITIZE_STRING);
		$id = filter_var($_POST["id"], FILTER_SANITIZE_STRING);

		$result = @mysqli_query($conn, "SELECT start, end FROM votes WHERE id = '$id'");
		if(mysqli_errno($conn))
		{
			echo '<error>';
			echo '<code>3</code>';
			echo '<message>'.mysqli_error($conn).'</message>';
			echo '</error>';
			return;
		}	

		if (mysqli_num_rows($result) == 1) 
		{
			$row = mysqli_fetch_assoc($result);
			$start = strtotime($row['start']);
			$end = strtotime($row['end']);
			if(time() < $start || time() > $end)
			{
				echo '<error>';
				echo '<code>5</code>';
				echo '<message>Time limits broken</message>';
				echo '</error>';
				return;
			}
		}
		else
		{
			echo '<error>';
			echo '<code>4</code>';
			echo '<message>Bad MySQL response</message>';
			echo '</error>';
			return;
		}

		if($science != '1' && $science != '2' && $science != '3' && $science != '4' && $science != '5')
			$science = '3';

		if($technic != '1' && $technic != '2' && $technic != '3' && $technic != '4' && $technic != '5')
			$technic = '3';

		if($creative != '1' && $creative != '2' && $creative != '3' && $creative != '4' && $creative != '5')
			$creative = '3';

		$user = $_SESSION['id'];

		$result = @mysqli_query($conn, "INSERT INTO rates 
			(user, vote, rate_science, rate_technic, rate_creative) 
			VALUES ('$user', '$id', '$science', '$technic', '$creative')
			ON DUPLICATE KEY UPDATE
			rate_science = '$science', rate_technic = '$technic', rate_creative  = '$creative'");

		if(mysqli_errno($conn))
		{
			echo '<error>';
			echo '<code>3</code>';
			echo '<message>'.mysqli_error($conn).'</message>';
			echo '</error>';
			return;
		}

		echo '<data>';
		echo 0;
		echo '</data>';
		return;
	}
?>